import type { RouteRecordRaw } from 'vue-router'

function Layout() {
  return import('@/layouts/index.vue')
}

const routes: RouteRecordRaw = {
  path: '/drugInfo',
  component: Layout,
  redirect: '/drugInfo/drug_list',
  name: 'drugInfo',
  meta: {
    title: '药品信息',
    i18n: 'route.breadcrumb.root',
    icon: 'drug',
    auth: ['p', 's'],
  },
  children: [
    {
      path: 'drug_list',
      name: 'drug_list',
      component: () => import('@/views/drug_tag/drug_list.vue'),
      meta: {
        title: '药品列表',
      },
    },
  ],
}

export default routes

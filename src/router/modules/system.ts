import type { RouteRecordRaw } from 'vue-router'

function Layout() {
  return import('@/layouts/index.vue')
}

const routes: RouteRecordRaw = {
  path: '/system',
  component: Layout,
  redirect: '/system/dict_manage',
  name: 'system',
  meta: {
    title: '系统管理',
    i18n: 'route.breadcrumb.root',
    icon: 'sys',
    auth: ['p'],
  },
  children: [
    {
      path: 'user_manage',
      name: 'user_manage',
      component: () => import('@/views/system/user.vue'),
      meta: {
        title: '用户管理',
      },
    },
    {
      path: 'dict_manage',
      name: 'dict_manage',
      component: () => import('@/views/system/dict.vue'),
      meta: {
        title: '字典管理',
      },
    },
  ],
}

export default routes

import type { RouteRecordRaw } from 'vue-router'

function Layout() {
  return import('@/layouts/index.vue')
}

const routes: RouteRecordRaw = {
  path: '/print_tpl',
  component: Layout,
  redirect: '/print_tpl/print_tpl_list',
  name: 'print_tpl',
  meta: {
    title: '打印模板',
    i18n: 'route.breadcrumb.root',
    icon: 'printTpl',
    auth: ['p'],
  },
  children: [
    {
      path: 'print_tpl_list',
      name: 'print_tpl_list',
      component: () => import('@/views/print_tpl/print_tpl_list.vue'),
      meta: {
        title: '模板列表',
      },
    },
    {
      path: 'print_tmpl',
      name: 'print_tmpl',
      component: () => import('@/views/print_tpl/print_tmpl.vue'),
      meta: {
        title: '模板设计',
      },
    }, {
      path: 'print_tpl_one',
      name: 'print_tpl_one',
      component: () => import('@/views/print_tpl/print_tpl_one.vue'),
      meta: {
        title: '模板设计1',
      },
    },
  ],
}

export default routes

import type { RouteRecordRaw } from 'vue-router'

function Layout() {
  return import('@/layouts/index.vue')
}

const routes: RouteRecordRaw = {
  path: '/store',
  component: Layout,
  redirect: '/store/store_list',
  name: 'store',
  meta: {
    title: '门店管理',
    i18n: 'route.breadcrumb.root',
    icon: 'store',
    auth: ['p'],
  },
  children: [
    {
      path: 'store_list',
      name: 'store_list',
      component: () => import('@/views/store/store_list.vue'),
      meta: {
        title: '门店列表',
      },
    },
    {
      path: 'store_account',
      name: 'store_account',
      component: () => import('@/views/store/store_account.vue'),
      meta: {
        title: '门店账户',
      },
    },
  ],
}

export default routes

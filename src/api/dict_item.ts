import request from './index'

export function page(params: any) {
  return request({
    url: '/v1/dictItem/page',
    method: 'get',
    params,
  })
}
export function list(params: any) {
  return request({
    url: '/v1/dictItem/list',
    method: 'get',
    params,
  })
}
export function listDictDataByDictHeaderValue(params: any) {
  return request({
    url: `/v1/listDictDataByDictHeaderValue?dictHeaderValue=${params}`,
    method: 'get',
  })
}
export function getById(id: any) {
  return request({
    url: `/v1/dictItem/${id}`,
    method: 'get',
  })
}
export function delById(id: any) {
  return request({
    url: `/v1/dictItem/${id}`,
    method: 'delete',
  })
}
export function delByIds(data: any) {
  return request({
    url: '/v1/dictItem/batchByIds',
    method: 'delete',
    data,
  })
}
export function update(data: any) {
  return request({
    url: '/v1/dictItem',
    method: 'put',
    data,
  })
}
export function add(data: any) {
  return request({
    url: '/v1/dictItem',
    method: 'post',
    data,
  })
}

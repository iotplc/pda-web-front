import api from '../index'

export default {
  // 保存条目
  createOrUpdate: (data: any) => api.post('/v1/storeAccountItemCreateOrUpdate', data),
  page: (data: any) => api.get('/v1/storeAccount/page', { params: data }),
  list: (data: any) => api.get('/v1/storeAccount/list', { params: data }),
  delBatch: (data: any) => api.delete('/v1/storeAccount/batchByIds', { data }),
  del: (id: any) => api.delete(`/v1/storeAccount/${id}`),
  resetPwd: (data: any) => api.post('/v1/storeAccount/editPwd', data),
  bindStoreUser: (data: any) => api.post('/v1/storeAccount/bindStoreUser', data),
  delStoreUserByTwoId: (data: any) => api.post('/v1/storeAccount/delStoreUserByTwoId', data),
}

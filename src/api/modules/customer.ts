import api from '../index'

export default {
  // 保存条目
  createOrUpdate: (data: any) => api.post('/v1/customerItemCreateOrUpdate', data),
  page: (data: any) => api.get('/v1/customer/page', { params: data }),
  notInStoreAccountPage: (data: any) => api.get('/v1/customer/notInStoreAccountPage', { params: data }),
  list: (data: any) => api.get('/v1/customer/list', { params: data }),
  delBatch: (data: any) => api.delete('/v1/customer/batchByIds', { data }),
  del: (id: any) => api.delete(`/v1/customer/${id}`),
}

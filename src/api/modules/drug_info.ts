import api from '../index'

export default {
  // 保存条目
  page: (data: any) => api.get('/v1/erp/goods/page', { params: data }),
  list: (data: any) => api.get('/v1/erp/goods/list', { params: data }),
}

import api from '../index'

export default {
  // 保存条目
  createOrUpdate: (data: any) => api.post('/v1/printTplItemCreateOrUpdate', data),
  page: (data: any) => api.get('/v1/printTpl/page', { params: data }),
  list: (data: any) => api.get('/v1/printTpl/list', { params: data }),
  delBatch: (data: any) => api.delete('/v1/printTpl/batchByIds', { data }),
  del: (id: any) => api.delete(`/v1/printTpl/${id}`),
  queryByName: (name: any) => api.get(`/v1/printTpl/byName/${name}`),
}

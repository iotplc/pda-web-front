import api from '../index'

export default {
  // 保存条目
  createOrUpdate: (data: any) => api.post('/v1/userPdaItemCreateOrUpdate', data),
  page: (data: any) => api.get('/v1/userPda/page', { params: data }),
  list: (data: any) => api.get('/v1/userPda/list', { params: data }),
  delBatch: (data: any) => api.delete('/v1/userPda/batchByIds', { data }),
  del: (id: any) => api.delete(`/v1/userPda/${id}`),
  resetPwd: (data: any) => api.post('/v1/authPda/editPwd', data),
}

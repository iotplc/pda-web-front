import request from './index'

export function page(params: any) {
  return request({
    url: '/v1/dictHeader/page',
    method: 'get',
    params,
  })
}
export function list(params: any) {
  return request({
    url: '/v1/dictHeader/list',
    method: 'get',
    params,
  })
}
export function getById(id: any) {
  return request({
    url: `/v1/dictHeader/${id}`,
    method: 'get',
  })
}
export function delById(id: any) {
  return request({
    url: `/v1/dictHeader/${id}`,
    method: 'delete',
  })
}
export function delByIds(data: any) {
  return request({
    url: '/v1/dictHeader/batchByIds',
    method: 'delete',
    data,
  })
}
export function update(data: any) {
  return request({
    url: '/v1/dictHeader',
    method: 'put',
    data,
  })
}
export function add(data: any) {
  return request({
    url: '/v1/dictHeader',
    method: 'post',
    data,
  })
}

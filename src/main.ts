import '@/utils/baidu'

import '@/utils/system.copyright'

import FloatingVue from 'floating-vue'
import 'floating-vue/dist/style.css'

import Message from 'vue-m-message'
import 'vue-m-message/dist/style.css'

import 'overlayscrollbars/overlayscrollbars.css'

import VXETable from 'vxe-table'

import { disAutoConnect } from 'vue-plugin-hiprint'

// 取消自动连接
import App from './App.vue'
import pinia from './store'
import router from './router'
import ui from './ui-provider'
import { setupI18n } from './locales'
import 'vxe-table/lib/style.css'

// 自定义指令
import { selectDictLabel } from './utils/customer'
import { listDictDataByDictHeaderValue } from './api/dict_item'
import directive from '@/utils/directive'

// 错误日志上报
import errorLog from '@/utils/error.log'

// 加载 svg 图标
import 'virtual:svg-icons-register'

// 加载 iconify 图标
import { downloadAndInstall } from '@/iconify'
import icons from '@/iconify/index.json'

import 'virtual:uno.css'

// 全局样式
import './style.css'
import '@/assets/styles/globals.scss'

disAutoConnect()

const app = createApp(App)
app.config.globalProperties.getDicts = listDictDataByDictHeaderValue
app.config.globalProperties.selectDictLabel = selectDictLabel
app.use(FloatingVue, {
  distance: 12,
})
app.use(Message)
app.use(pinia)
app.use(router)
app.use(ui)
app.use(VXETable)
app.use(setupI18n())
directive(app)
errorLog(app)
if (icons.isOfflineUse) {
  for (const info of icons.collections) {
    downloadAndInstall(info)
  }
}

app.mount('#app')

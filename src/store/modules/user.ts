import useSettingsStore from './settings'
import useTabbarStore from './tabbar'
import useRouteStore from './route'
import useMenuStore from './menu'
import router from '@/router'
import apiUser from '@/api/modules/user'
import storage from '@/utils/storage'

const useUserStore = defineStore(
  // 唯一ID
  'user',
  () => {
    const settingsStore = useSettingsStore()
    const tabbarStore = useTabbarStore()
    const routeStore = useRouteStore()
    const menuStore = useMenuStore()

    const accountType = ref(storage.local.get('accountType') ?? '')
    const account = ref(storage.local.get('account') ?? '')
    const token = ref(storage.local.get('token') ?? '')
    const failure_time = ref(storage.local.get('failure_time') ?? '')
    const avatar = ref(storage.local.get('avatar') ?? '')
    const permissions = ref<string[]>([])
    const storeList = ref(storage.local.get('storeList') ?? '')
    const userInfo = ref(storage.local.get('userInfo') ?? '')
    const isLogin = computed(() => {
      let retn = false
      if (token.value) {
        if (new Date().getTime() < Number.parseInt(failure_time.value) * 1000) {
          retn = true
        }
      }
      return retn
    })

    // 登录
    async function login(data: {
      account: string
      password: string
      accountType: string
    }) {
      const res = await apiUser.login(data)
      storage.local.set('accountType', data.accountType)
      storage.local.set('account', res.data.account)
      storage.local.set('token', res.data.token)
      storage.local.set('failure_time', res.data.failure_time)
      storage.local.set('avatar', res.data.avatar)
      storage.local.set('userInfo', JSON.stringify(res.data.userInfo))
      storage.local.set('storeList', JSON.stringify(res.data.storeList))
      account.value = res.data.account
      accountType.value = data.accountType
      token.value = res.data.token
      failure_time.value = res.data.failure_time
      avatar.value = res.data.avatar
      userInfo.value = JSON.stringify(res.data.userInfo)
      storeList.value = JSON.stringify(res.data.storeList)
    }
    // 登出
    async function logout(redirect = router.currentRoute.value.fullPath) {
      storage.local.remove('account')
      storage.local.remove('accountType')
      storage.local.remove('token')
      storage.local.remove('failure_time')
      storage.local.remove('avatar')
      storage.local.remove('userInfo')
      storage.local.remove('storeList')
      account.value = ''
      accountType.value = ''
      token.value = ''
      failure_time.value = ''
      avatar.value = ''
      permissions.value = []
      userInfo.value = ''
      storeList.value = ''
      tabbarStore.clean()
      routeStore.removeRoutes()
      menuStore.setActived(0)
      router.push({
        name: 'login',
        query: {
          ...(router.currentRoute.value.path !== settingsStore.settings.home.fullPath && router.currentRoute.value.name !== 'login' && { redirect }),
        },
      })
    }
    // 获取权限
    async function getPermissions() {
      const res = await apiUser.permission({ accountType: accountType.value })
      permissions.value = res.data
      return permissions.value
    }
    // 获取偏好设置
    async function getPreferences() {
      const res = await apiUser.preferences()
      settingsStore.updateSettings(JSON.parse(res.data.preferences || '{}'), true)
    }
    // 修改密码
    async function editPassword(data: {
      password: string
      newpassword: string
    }) {
      await apiUser.passwordEdit(data)
    }

    return {
      account,
      accountType,
      token,
      avatar,
      permissions,
      userInfo,
      storeList,
      isLogin,
      login,
      logout,
      getPermissions,
      getPreferences,
      editPassword,
    }
  },
)

export default useUserStore

// 回显数据字典
export function selectDictLabel(datas: any, value: any) {
  const actions: any = []
  Object.keys(datas).map((key) => {
    if (datas[key].value === (`${value}`)) {
      actions.push(datas[key].label)
      return true
    }
    return false
  })
  return actions.join('')
}
